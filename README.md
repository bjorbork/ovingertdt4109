# Øvingsrepo TDT4109 2023

### Øvings oppgaver

-   [Øving 1](./Ovinger/Oving1/_Oving1.ipynb)
-   [Øving 2](./Ovinger/Oving2/_Oving2.ipynb)
-   [Øving 3](./Ovinger/Oving3/_Oving3.ipynb)
-   Øving 4 (kommer senere)
-   Øving 5 (kommer senere)
-   Øving 6 (kommer senere)
-   Øving 7 (kommer senere)
-   Øving 8 (kommer senere)

### Løsningsforslag

_Kommer senere_
